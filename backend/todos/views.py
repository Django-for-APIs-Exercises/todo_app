from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
)
from .models import Todo
from rest_framework.serializers import Serializer
from .serializers import TodoSerializer

# Create your views here.

class TodoListView(ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

class TodoDetailsView(RetrieveAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
