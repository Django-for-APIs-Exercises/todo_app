from django.urls import path
from .views import (
    TodoListView,
    TodoDetailsView,
)

urlpatterns = [
    path('<int:pk>/', TodoDetailsView().as_view()),
    path('', TodoListView().as_view())
]